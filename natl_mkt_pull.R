##############################################################################
# Project: CHS - Joint Replacement - Bayfront
# Date: 8/14/14
# Author: Sam Bussmann
# Description: Pull data from national market database for scoring
# Notes: Used this 
##############################################################################

zips_pre<-read.csv("service_areas.csv",header=T)
dimnames(zips_pre)[[2]]<-c("zipcode","ServiceArea","Hospital")
zips<-zips_pre[["zipcode"]]

zips1<-NULL
for (i in 1:length(zips)){
  if (i!=length(zips)) zips1<-paste0(zips1,"\'",zips[i],"\',") else
    zips1<-paste0(zips1,"\'",zips[i],"\'")
}

library(RODBC)
sql1<-odbcConnect("TncmSql1")
system.time(
  addr<-as.data.frame(sqlQuery(sql1,
                               paste0("SELECT *
                                      FROM [NationalMarket].[dbo].[NationalPerson] np
left join [NationalMarket].[dbo].[NationalHousehold] nh on np.familyid=nh.familyid
                                      WHERE  (nh.zipcode IN (", zips1 ,"))")
                               ,errors=T,stringsAsFactors = F))
)

addr2<-merge(addr,zips_pre[!duplicated(zips),],by=c("zipcode"),all.x=T)

save(addr2,file="natlmkt.RData")
